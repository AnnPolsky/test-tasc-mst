import { useContext, useEffect, useRef, useState } from "react";
import { Statistic } from ".";

export const Menu: React.FC<{}> = () => {
  const statistic = useContext(Statistic);
  const [winStatus, setWinStatus] = useState<string>("");
  const [compWinsCount, setCompWinsCount] = useState<number>(0);
  const [userWinsCount, setUserWinsCount] = useState<number>(0);

  useEffect(() => {
    statistic.lastWinner.subscribe((value) => {
      setWinStatus(value);
      const inc = (vals: number) => {
          return vals+1;
      };
      if (value === "Winner: X") {
        setUserWinsCount(inc);
      } else {
        setCompWinsCount(inc);
      }
    });
  }, []);

  return (
    <div className="menu">
      <div>{`computer won: ${compWinsCount} games`}</div>
      <div>{`user won: ${userWinsCount} games`}</div>
      <div>{winStatus}</div>
    </div>
  );
};
