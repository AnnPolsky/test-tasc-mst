import React, { Fragment } from "react";
import { StateData } from "./api/state";

export const Stats: React.FC<{ stateData: typeof StateData }> = ({
  stateData,
}) => (
  <div className="state">
    All games:
    {stateData
      .slice(0, stateData.length - 1)
      .map(({ state, winner }: any, index: number) => (
        <>
          <div className="game">game: {index + 1}</div>
          <div key={index} className="board">
            board:
            {state.map((elem: number, key: number) => (
              <Fragment key={key + winner}>{elem ? "X " : "O "}</Fragment>
            ))}
            <div>
              win:{" "}
              {winner === true ? "X " : winner === false ? "O " : "No winner"}
            </div>
          </div>
        </>
      ))}
  </div>
);
