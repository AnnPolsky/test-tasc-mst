import type { NextApiRequest, NextApiResponse } from "next";

interface IStateData {
  userTurns?: number[];
  compTurns?: number[];
  state: number[];
  winner: "Computer" | "User" | "No winner" | null;
}

export const StateData: IStateData[] = [
  {
    winner: null,
    state: [],
    userTurns: [],
    compTurns: [],
  },
];

export default function handler(req: NextApiRequest, res: NextApiResponse) {
  if (req.method === "POST") {
    const { userTurns, compTurns, winner, state } = req.body.data as IStateData;
    const oldData = StateData.pop() || {state: [], winner: null};
    oldData.state = state;
    oldData.winner = winner;
    StateData.push(oldData);

    if (winner!==null)
    StateData.push({
      state: [],
      winner: null
    });
    console.error({StateData}, StateData[0].state);
  }
  res.status(200).json(StateData);
}
