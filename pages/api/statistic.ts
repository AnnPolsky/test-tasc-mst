import type { NextApiRequest, NextApiResponse } from "next";

interface IStatData {
  lastWinner: string;
  compWinners: number;
  userWinners: number;
}

export const StatisticData: IStatData = {
    lastWinner: '',
    userWinners: 0,
    compWinners: 0,
  };

export default function handler(req: NextApiRequest, res: NextApiResponse) {
  if (req.method === "POST") {
    const { lastWinner, userWinners, compWinners } = req.body.data;
    StatisticData.lastWinner = lastWinner;
    StatisticData.compWinners = compWinners;
    StatisticData.userWinners = userWinners;
  }
  res.status(200).json(StatisticData);
}
