import { createStore, combineReducers, applyMiddleware } from "redux";
import {
  combineEpics,
  createEpicMiddleware,
  ofType,
  StateObservable,
} from "redux-observable";
import { from, Observable, pipe } from "rxjs";
import { Action } from "rxjs/internal/scheduler/Action";
import {
  concatMap,
  filter,
  map,
  mergeMap,
  withLatestFrom,
} from "rxjs/operators";
import { postState } from ".";
import { StateData } from "./api/state";

export const USER_TURN = "USER_TURN";
export const COMP_TURN = "COMP_TURN";
export const CHANGE_PLAYER = "CHANGE_PLAYER";
export const IS_WIN = "IS_WIN";

const getCompTurnIndex = (state: Array<boolean | null>): number | null => {
  const emptyElements = state.filter((elem) => elem === null);
  if (emptyElements.length === 0) return null;
  let t;
  do {
    t = Math.floor(Math.random() * state.length);
  } while (state[t] !== null);
  return t;
};

export const calculateWinner = (
  squares: Array<boolean | null>
): boolean | null => {
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];

  for (let i = 0; i < lines.length; i++) {
    const [a, b, c] = lines[i];
    if (squares[a] === squares[b] && squares[a] === squares[c]) {
      return squares[a];
    }
  }
  return null;
};

const newBoard = (state: Array<boolean | null>, { type, payload }: any) => {
  if (payload?.index === null) return state;
  switch (type) {
    case COMP_TURN:
    case USER_TURN:
      const newState = state.slice();
      newState[payload.index] = payload.value;
      return newState;

    default:
      return new Array(9).fill(null);
  }
};

const rootReducer = combineReducers({ newBoard });

const boardAction = (
  type: "COMP_TURN" | "USER_TURN",
  index: number | null,
  value: boolean
): {
  type: string;
  payload: {
    index: number | null;
    value: boolean;
  };
} => ({
  type,
  payload: {
    index,
    value,
  },
});

export const userTurnAction = (index: number | null) =>
  boardAction(USER_TURN, index, true);
export const compTurnAction = (index: number | null) =>
  boardAction(COMP_TURN, index, false);


const boardEpic = (
  action$: Observable<Action<any>>,
  state$: StateObservable<{ newBoard: any }>
) => {
  return action$.pipe(
    map((x) => {
      postState({state: state$.value.newBoard, winner: calculateWinner(state$.value.newBoard)});
      return x;
    }),
    filter((action: any) => action.type === USER_TURN && calculateWinner(state$.value.newBoard)===null),
    map((action: any) =>
      compTurnAction(getCompTurnIndex(state$.value.newBoard))
    )
  );
};



const rootEpic = combineEpics(boardEpic);

const epicMiddleware = createEpicMiddleware();
export const store = createStore(rootReducer, applyMiddleware(epicMiddleware));

epicMiddleware.run(rootEpic);
