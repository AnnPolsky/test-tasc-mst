const baseUrl = `http://localhost:3000/api/`;
const headers = {
  "Content-Type": "application/json",
};

export const getFetchRequest = (url: string): Promise<any> =>
  fetch(`${baseUrl}${url}`, {
    method: "GET",
    headers,
  }).then((response) => response.json());

export const postFetchRequest = (url: string, data: any): Promise<any> => 
  fetch(`${baseUrl}${url}`, {
    method: "POST",
    headers,
    body: JSON.stringify({
      data,
    }),
  }).then((response) => response.json());
