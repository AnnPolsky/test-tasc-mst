import '../styles/globals.css'
import './Area.css';
import './Menu.css';
import './Stats.css';
import type { AppProps } from 'next/app'

function MyApp({ Component, pageProps }: AppProps) {
  return <Component {...pageProps} />
}
export default MyApp
