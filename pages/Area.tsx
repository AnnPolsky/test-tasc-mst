import { useContext, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Statistic } from ".";
import { userTurnAction, calculateWinner } from "./store";

const PlayerMap = ["O", "X"];

interface ISquare {
  value: boolean | null;
  onClick: () => void;
}

const Square: React.FC<ISquare> = ({ value, onClick }: ISquare) => {
  return (
    <button className="square" onClick={onClick}>
      {value !== null && PlayerMap[+value]}
    </button>
  );
};

interface IState {
  newBoard: Array<boolean | null>;
}

export const Area: React.FC<{}> = () => {
  const statistic = useContext(Statistic);
  const rowsForRender: JSX.Element[] = [];
  const state = useSelector<IState>((state: IState) => state.newBoard) as Array<
    boolean | null
  >;
  const dispatch = useDispatch();

  const SIZE = 3;
  const player = true;
  const [status, setStatus] = useState<string>("");

  const handlerClick = (index: number) => {
    if (calculateWinner(state) !== null || state[index] !== null) {
      return;
    }

    dispatch(userTurnAction(index));
  };

  useEffect(() => {
    const winner = calculateWinner(state);

    if (winner !== null) {
      const winStatus = "Winner: " + PlayerMap[+winner];
      if (winner === true) {
        statistic.userWinners.next();
      } else {
        statistic.compWinners.next();
      }
      statistic.lastWinner.next(winStatus);
      setStatus(winStatus);
    } else {
      setStatus("Next player: " + PlayerMap[+player]);
    }
  }, [state]);

  for (let i = 0; i < SIZE; i++) {
    const cols: JSX.Element[] = [];
    for (let j = 0; j < SIZE; j++) {
      const index = i * SIZE + j;
      cols.push(
        <Square
          key={`square_${index}`}
          value={state[index]}
          onClick={() => handlerClick(index)}
        />
      );
    }
    rowsForRender.push(
      <div key={`row_${i}`} className="board-row">
        {cols}
      </div>
    );
  }

  return (
    <div>
      <div className="status">{status}</div>
      {rowsForRender}
      <button onClick={() => dispatch({ type: "" })}>Начать заного</button>
    </div>
  );
};
